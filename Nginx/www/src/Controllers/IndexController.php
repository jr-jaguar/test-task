<?php

namespace src\Controllers;

use src\Models\Repository\ClientRepository;
use src\Views\View;

class IndexController
{

    public function __construct(private ClientRepository $clientRepository)
    {
    }

    public function index(): void
    {

        $category = $_GET['category'] ?? null;
        $gender = $_GET['gender'] ?? null;
        $minAge = $_GET['minAge'] ?? null;
        $maxAge = $_GET['maxAge'] ?? null;

        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perPage = 10;

        $filteredClients = $this->clientRepository->getByFiltersWithPagination($category, $gender, $minAge, $maxAge, $page, $perPage);
        $categories = $this->clientRepository->getAllCategories();
        $totalCount = $filteredClients['totalCount'];

        $totalPages = ceil($totalCount / $perPage);

        View::render('index', [
            'data' => $filteredClients['data'],
            'currentPage' => $page,
            'totalPages' => $totalPages,
            'categories' => $categories
        ]);
    }

}