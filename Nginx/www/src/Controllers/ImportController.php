<?php

namespace src\Controllers;

use src\Services\CsvImportService;
use src\Views\View;

class ImportController
{

    public function __construct(private CsvImportService $csvImportService)
    {
    }

    public function import():void
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            try {
                $csvFilePath = $_FILES['csvFile']['tmp_name'];

                $this->csvImportService->importClientsFromCsv($csvFilePath);

                echo "Import successful!";
                header('Location: /');
                exit();
            } catch (\Exception $e) {
                echo "Import failed: " . $e->getMessage();
            }
        }
        View::render('import');
    }
}