<?php

namespace src\Controllers;

use src\Services\CsvExportService;

class ExportController
{
    public function __construct(private CsvExportService $csvExportService)
    {
    }

    public function export(): void
    {
        $ids = isset($_POST['selected_ids']) ? $_POST['selected_ids'] : [];

        $filePath = '/tmp/export.csv';
        $ids = json_decode($ids);
        $this->csvExportService->exportToCsv($filePath, $ids);

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="exported_data.csv"');
        readfile($filePath);
    }
}