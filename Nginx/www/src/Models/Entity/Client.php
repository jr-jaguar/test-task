<?php

namespace src\Models\Entity;

use src\Models\Repository\ClientRepository;

class Client
{
    private $id;
    private $category;
    private $firstname;
    private $lastname;
    private $email;
    private $gender;
    private $birthDate;

    public function getId():int
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function setCategory( $category): void
    {
        $this->category = $category;
    }

    public function getFirstname():string
    {
        return $this->firstname;
    }

    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }

    public function getLastname():string
    {
        return $this->lastname;
    }

    public function setLastname($lastname): void
    {
        $this->lastname = $lastname;
    }

    public function getEmail():string
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getGender():string
    {
        return $this->gender;
    }

    public function setGender($gender): void
    {
        $this->gender = $gender;
    }

    public function getBirthDate():string
    {
        return $this->birthDate;
    }

    public function setBirthDate($birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    public function save(ClientRepository $repository)
    {
        $repository->save($this);
    }

}