<?php

namespace src\Models\Repository;
use src\Infrastructure\Database;
use src\Models\Entity\Category;
class CategoryRepository
{
    private $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function create(Category $category)
    {
        $query = "INSERT INTO categories (name) VALUES (:name)";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':name', $category->getName());
        $stmt->execute();

        $category->setId($this->db->getConnection()->lastInsertId());
    }

    public function delete(Category $category)
    {
        $query = "DELETE FROM categories WHERE id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':id', $category->getId());
        $stmt->execute();
    }



}