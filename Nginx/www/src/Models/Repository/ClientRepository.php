<?php

namespace src\Models\Repository;

    use src\Infrastructure\Database;
    use src\Models\Entity\Client;

class ClientRepository
{

    public function __construct(private Database $db)
    {
    }

    public function getAll()
    {
        $query = $this->db->query("SELECT * FROM clients");
        return $query->fetchAll(Database::FETCH_MODE, 'src\Models\Entity\Client');
    }

    public function getByFilters($category, $gender, $minAge, $maxAge)
    {
        $query = "SELECT * FROM clients WHERE ";

        if ($category) {
            $query .= " AND category = :category";
        }

        if ($gender) {
            $query .= " AND gender = :gender";
        }

        if ($minAge && $maxAge) {
            $query .= " AND birthDate BETWEEN :minAge AND :maxAge";
        }

        $stmt = $this->db->prepare($query);

        if ($category) {
            $stmt->bindValue(':category', $category);
        }

        if ($gender) {
            $stmt->bindValue(':gender', $gender);
        }

        if ($minAge && $maxAge) {
            $stmt->bindValue(':minAge', $minAge);
            $stmt->bindValue(':maxAge', $maxAge);
        }

        $stmt->execute();

        return $stmt->fetchAll(Database::FETCH_MODE, 'src\Models\Entity\Client');
    }

    public function save(Client $client) {
        $stmt = $this->db->query("INSERT INTO clients (category, firstname, lastname, email, gender, birthDate) VALUES (?, ?, ?, ?, ?, ?)",
            [$client->getCategory(), $client->getFirstname(), $client->getLastname(), $client->getEmail(), $client->getGender(), $client->getBirthDate()]);
    }
    public function getTotalCount()
    {
        $query = "SELECT COUNT(*) as count FROM clients";
        $stmt = $this->db->query($query);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $result['count'];
    }
    public function getPaginatedData($page, $perPage)
    {
        $offset = ($page - 1) * $perPage;

        $query = "SELECT * FROM clients LIMIT :perPage OFFSET :offset";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':perPage', $perPage, \PDO::PARAM_INT);
        $stmt->bindValue(':offset', $offset, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(Database::FETCH_MODE, 'src\Models\Entity\Client');
    }

    public function getByIds($ids){
        $query = "SELECT * FROM clients WHERE id IN (" . implode(',', $ids) . ")";
        $stmt = $this->db->query($query);
        return $stmt->fetchAll(Database::FETCH_MODE);

    }

    public function getAllCategories(){
            $query = "SELECT DISTINCT category FROM clients";
            $stmt = $this->db->query($query);

            return $stmt->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function getByFiltersWithPagination($category, $gender, $minAge, $maxAge, $page, $perPage)
    {
        $offset = ($page - 1) * $perPage;

        $countQuery = "SELECT COUNT(*) as count FROM clients WHERE 1";
        $dataQuery = "SELECT * FROM clients WHERE 1";

        if ($category) {
            $countQuery .= " AND category = :category";
            $dataQuery .= " AND category = :category";
        }

        if ($gender) {
            $countQuery .= " AND gender = :gender";
            $dataQuery .= " AND gender = :gender";
        }

        if ($minAge && $maxAge) {
            $countQuery .= " AND TIMESTAMPDIFF(YEAR, birthDate, CURDATE()) BETWEEN :minAge AND :maxAge";
            $dataQuery .= " AND TIMESTAMPDIFF(YEAR, birthDate, CURDATE()) BETWEEN :minAge AND :maxAge";
        }

        $stmtCount = $this->db->prepare($countQuery);

        if ($category) {
            $stmtCount->bindValue(':category', $category);
        }

        if ($gender) {
            $stmtCount->bindValue(':gender', $gender);
        }

        if ($minAge && $maxAge) {
            $stmtCount->bindValue(':minAge', $minAge);
            $stmtCount->bindValue(':maxAge', $maxAge);
        }

        $stmtCount->execute();
        $totalCount = $stmtCount->fetchColumn();

        $dataQuery .= " LIMIT :perPage OFFSET :offset";
        $stmtData = $this->db->prepare($dataQuery);

        if ($category) {
            $stmtData->bindValue(':category', $category);
        }

        if ($gender) {
            $stmtData->bindValue(':gender', $gender);
        }

        if ($minAge && $maxAge) {
            $stmtData->bindValue(':minAge', $minAge);
            $stmtData->bindValue(':maxAge', $maxAge);
        }

        $stmtData->bindValue(':perPage', $perPage, \PDO::PARAM_INT);
        $stmtData->bindValue(':offset', $offset, \PDO::PARAM_INT);

        $stmtData->execute();
        $data = $stmtData->fetchAll(Database::FETCH_MODE, 'src\Models\Entity\Client');

        return [
            'data' => $data,
            'totalCount' => $totalCount,
        ];
    }
}
