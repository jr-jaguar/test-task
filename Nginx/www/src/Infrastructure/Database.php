<?php

namespace src\Infrastructure;

use PDO;

class Database
{
    const FETCH_MODE = PDO::FETCH_CLASS;
    private static $instance;
    private PDO $connection;

    private function __construct()
    {
        $config = require_once __DIR__ . '/../../config/config.php';

        try {
            $this->connection = new \PDO("mysql:host={$config['db_host']};dbname={$config['db_name']}", $config['db_user'], $config['db_password']);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    public static function getInstance(): Database
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getConnection(): PDO
    {
        return $this->connection;
    }

    public function execute($query, $params = [])
    {
        $statement = $this->connection->prepare($query);
        return $statement->execute($params);
    }

    public function query($query, $params = []): false|\PDOStatement
    {
        $statement = $this->connection->prepare($query);
        $statement->execute($params);
        return $statement;
    }

    public function prepare($query, $params = []): false|\PDOStatement
    {
        $statement = $this->connection->prepare($query, $params);
        return $statement;
    }
}