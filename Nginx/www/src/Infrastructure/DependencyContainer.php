<?php

namespace src\Infrastructure;

class DependencyContainer
{
    private $instances = [];

    public function __construct(private array $config)
    {
    }

    public function get($class)
    {
        if (!isset($this->instances[$class])) {
            if (isset($this->config[$class])) {
                $this->instances[$class] = $this->config[$class]($this);
            } else {
                $this->instances[$class] = new $class($this);
            }
        }

        return $this->instances[$class];
    }
}