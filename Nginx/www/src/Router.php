<?php

namespace src;

class Router
{
    public function __construct(private array $routes)
    {
    }

    public function route($uri) {
        return $this->routes[$uri] ?? null;
    }
}