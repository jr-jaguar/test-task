<?php

namespace src\Services;
use src\Models\Repository\ClientRepository;
use src\Models\Entity\Client;
class CsvImportService
{
    public function __construct(private ClientRepository $clientRepository)
    {
    }

    public function importClientsFromCsv($csvFilePath) {
        $file = fopen($csvFilePath, 'r');

        if (!$file) {
            throw new \Exception("Unable to open CSV file.");
        }

        $headers = fgetcsv($file);

        while (($data = fgetcsv($file)) !== false) {
            $row = array_combine($headers, $data);

            $client = new Client();
            $client->setCategory($row['category']);
            $client->setFirstname($row['firstname']);
            $client->setLastname($row['lastname']);
            $client->setEmail($row['email']);
            $client->setGender($row['gender']);
            $client->setBirthDate($row['birthDate']);

            $this->clientRepository->save($client);
        }

        fclose($file);
    }
}