<?php

namespace src\Services;

use src\Models\Repository\ClientRepository;

class CsvExportService
{
    public function __construct(private ClientRepository $clientRepository)
    {
    }

    public function exportToCsv($filePath, $ids){

        $data = $this->clientRepository->getByIds($ids);

        $handle = fopen($filePath, 'w');

        fputcsv($handle, array_keys((array) $data[0]));

        foreach ($data as $row) {
            fputcsv($handle, (array) $row);
        }

        fclose($handle);
    }
}