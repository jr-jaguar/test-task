<?php

namespace src\Views;

class View
{
    public static function render($viewName, $data=null): void
    {
        include realpath(__DIR__ . "/templates/$viewName.php");
    }
}