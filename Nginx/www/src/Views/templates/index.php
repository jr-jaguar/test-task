<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index Page</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
            margin-top: 20px;
        }

        table, th, td {
            border: 1px solid black;
        }

        th, td {
            padding: 10px;
            text-align: left;
        }
    </style>
</head>
<body>
<h1>Data Table</h1>

<form action="/" method="get">
    <label for="category">Category:</label>
    <select name="category" id="category">
        <option value="">All</option>
        <?php foreach ($data['categories'] as $category):?>
        <option value="<?= $category;?>" <?= ($_GET['category'] ?? null) === $category ? 'selected' : ''; ?>><?= $category;?></option>
        <?php endforeach;?>
    </select>

    <label for="gender">Gender:</label>
    <select name="gender" id="gender">
        <option value="" <?= empty($_GET['gender']) ? 'selected' : ''; ?>>All</option>
        <option value="Male" <?= ($_GET['gender'] ?? null) === 'Male' ? 'selected' : ''; ?>>Male</option>
        <option value="Female" <?= ($_GET['gender'] ?? null) === 'Female' ? 'selected' : ''; ?>>Female</option>
    </select>

    <label for="minAge">Min Age:</label>
    <input type="number" name="minAge" id="minAge" value="<?= isset($_GET['minAge']) ? $_GET['minAge'] : ''; ?>" />

    <label for="maxAge">Max Age:</label>
    <input type="number" name="maxAge" id="maxAge" value="<?= isset($_GET['maxAge']) ? $_GET['maxAge'] : ''; ?>" />

    <button type="submit">Apply Filters</button>
</form>

<div>
    <form action="/import" method="get">
        <button type="submit">Import</button>
    </form>

    <form action="/export" method="post">
        <input type="hidden" name="selected_ids" id="selected_ids" value="<?= htmlspecialchars(
                json_encode(
                        array_map(function ($client) {
                        return $client->getId();
                        }, $data['data'])
                        )) ?>" />
        <button type="submit">Экспорт</button>
    </form>
</div>

<?php if (empty($data['data'])): ?>
    <p>No data available.</p>
<?php else: ?>
    <table>
        <thead>
        <tr>
            <th>Category</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>Gender</th>
            <th>BirthDate</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data['data'] as $row): ?>
            <tr>
                <td><?= htmlspecialchars($row->getCategory()) ?></td>
                <td><?= htmlspecialchars($row->getFirstname()) ?></td>
                <td><?= htmlspecialchars($row->getLastname()) ?></td>
                <td><?= htmlspecialchars($row->getEmail()) ?></td>
                <td><?= htmlspecialchars($row->getGender()) ?></td>
                <td><?= htmlspecialchars($row->getBirthDate()) ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php $queryParams = http_build_query($_GET);?>
    <div class="pagination">
        <?php for ($i = 1; $i <= $data['totalPages']; $i++): ?>
            <a href="?page=<?= $i .'&'. $queryParams ?>" <?= ($data['currentPage'] == $i) ? 'class="active"' : ''; ?>>
                <?= $i ?>
            </a>
        <?php endfor; ?>
    </div>
<?php endif; ?>
</body>
</html>
