<?php
spl_autoload_register(function ($class) {
    $base_dir = __DIR__ . '/';
    $file = __DIR__ . '/' . str_replace('\\', '/', $class) . '.php';
    if (file_exists($file)) {
        require_once $file;
    }
});