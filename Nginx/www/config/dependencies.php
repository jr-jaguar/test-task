<?php

use src\Controllers\ExportController;
use src\Controllers\ImportController;
use src\Controllers\IndexController;
use src\Infrastructure\Database;
use src\Models\Repository\ClientRepository;
use src\Router;
use src\Services\CsvExportService;
use src\Services\CsvImportService;

return [
    Router::class => function () {
        $base_dir = __DIR__ . '/routes.php';
        $routers = require_once $base_dir;
        return new Router($routers);
    },
    CsvImportService::class => function ($container) {
        $clientRepository = $container->get(ClientRepository::class);
        return new CsvImportService($clientRepository);
    },
    CsvExportService::class => function ($container) {
        $clientRepository = $container->get(ClientRepository::class);
        return new CsvExportService($clientRepository);
    },
    ClientRepository::class => function ($container) {
        $db = $container->get(Database::class);
        return new ClientRepository($db);
    },
    ImportController::class => function ($container) {
        $csvImportService = $container->get(CsvImportService::class);
        return new ImportController($csvImportService);
    },
    ExportController::class => function ($container) {
        $csvExportService = $container->get(CsvExportService::class);
        return new ExportController($csvExportService);
    },
    IndexController::class => function ($container) {
        $clientRepository = $container->get(ClientRepository::class);
        return new IndexController($clientRepository);
    },

    Database::class => function () {
        return Database::getInstance();
    },
];