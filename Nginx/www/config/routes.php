<?php
return [
    '/' => ['src\Controllers\IndexController', 'index'],
    '/export' => ['src\Controllers\ExportController', 'export'],
    '/import' => ['src\Controllers\ImportController', 'import'],
];