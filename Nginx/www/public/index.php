<?php
require_once '../Autoloader.php';

use src\Infrastructure\DependencyContainer;
use src\Router;

$dependenciesConfig = require_once '../config/dependencies.php';

$container = new DependencyContainer($dependenciesConfig);

$uri = $_SERVER['REQUEST_URI'];
$uri = strtok($uri, '?');

$router = $container->get(Router::class);

$routeInfo = $router->route($uri);
if ($routeInfo) {
    [$controllerClass, $method] = $routeInfo;
    $controller = $container->get($controllerClass);
    $controller->$method();
} else {
    echo "404 Not Found";
}